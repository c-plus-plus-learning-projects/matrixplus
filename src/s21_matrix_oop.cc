#include "s21_matrix.h"

S21Matrix::S21Matrix() {
    _rows = 0;
    _cols = 0;
}

S21Matrix::S21Matrix(int rows, int cols) {
    _rows = rows;
    _cols = cols;
    if (_rows < 1 || _cols < 1) throw invalid_argument(messages(8));
    s21_create_matrix();
}

S21Matrix::S21Matrix(const S21Matrix& other) {
    s21_copy(other);
}

S21Matrix::S21Matrix(S21Matrix&& other) {
    s21_copy(other);
}

S21Matrix::~S21Matrix() {}

void S21Matrix::s21_create_matrix() {
    for (int i = 0; i < _rows; i++) {
        _matrix.push_back(vector<double>(_cols, 0.0));
    }
}

bool S21Matrix::eq_matrix(const S21Matrix& other) {
    int res = true;
    if (s21_check_for_volid(other, J_OP)) {
        for (int i = 0; i < _rows && res == true; i++) {
            for (int j = 0; j < _cols; j++) {
                if (s21_fabs(_matrix[i][j] - other._matrix[i][j]) > EPS) res = false;
            }
        }
    } else {
        res = false;
    }
    return res;
}

void S21Matrix::sum_matrix(const S21Matrix& other) {
    return s21_sum_sub_mult_matrix(other, SUM, 0);
}

void S21Matrix::sub_matrix(const S21Matrix& other) {
    return s21_sum_sub_mult_matrix(other, SUB, 0);
}

void S21Matrix::mul_number(const double num) {
    return s21_sum_sub_mult_matrix(*this, MULT, num);
}

void S21Matrix::mul_matrix(const S21Matrix& other) {
    s21_check_for_volid(other, MATRIX_MULT_OP);
    S21Matrix res(_rows, other._cols);
    for (int i = 0; i < _rows; i++) {
        for (int j = 0; j < other._cols; j++) {
            for (int k = 0; k < other._rows; k++) {
                res._matrix[i][j] += _matrix[i][k] * other._matrix[k][j];
            }
        }
    }
    *this = res;
}

S21Matrix S21Matrix::transpose() {
    S21Matrix res(_cols, _rows);
    for (int i = 0; i < _rows; i++) {
        for (int j = 0; j < _cols; j++) {
            res._matrix[j][i] = _matrix[i][j];
        }
    }
    return res;
}

S21Matrix S21Matrix::calc_complements() {
    s21_check_for_volid(*this, CALC);
    S21Matrix res(_rows, _cols);
    for (int i = 0; i < _rows; i++) {
        for (int j = 0; j < _cols; j++) {
            S21Matrix minor = s21_minor_matrix(i, j);
            res._matrix[i][j] = s21_1_or_minus_1(i + j) * minor.determinant();
        }
    }
    return res;
}

double S21Matrix::determinant() {
    double res = 0;
    s21_check_for_volid(*this, SQUARE);
    if (_rows == 1) {
        res = _matrix[0][0];
    } else if (_rows == 2) {
            res = _matrix[0][0] * _matrix[1][1] -
                    _matrix[1][0] * _matrix[0][1];
    } else {
        for (int i = 0; i < _cols; i++) {
            S21Matrix minor_matrix = s21_minor_matrix(0, i);
            res += s21_1_or_minus_1(i) * _matrix[0][i] * minor_matrix.determinant();
        }
    }
    return res;
}

S21Matrix S21Matrix::inverse_matrix() {
    s21_check_for_volid(*this, INVERSE);
    double d = determinant();
    S21Matrix C(_rows, _cols);
    if (_rows == 1) {
        C._matrix[0][0] = 1 / d;
    } else {
        S21Matrix tm = calc_complements().transpose();
        tm.mul_number(1 / d);
        C = tm;
    }
    return C;
}

int S21Matrix::get_rows() { return _rows; }

int S21Matrix::get_cols() { return _cols; }

void S21Matrix::set_rows(int n_rows) {
    if (n_rows < 1) throw invalid_argument(messages(7));
    if (n_rows < _rows) {
        while (n_rows != _rows) {
            _matrix.pop_back();
            _rows--;
        }
    } else if (n_rows > _rows) {
        while (n_rows != _rows) {
            _matrix.push_back(vector<double>(_cols, 0.0));
            _rows++;
        }
    }
}

void S21Matrix::set_cols(int n_cols) {
    if (n_cols < 1) throw invalid_argument(messages(6));
    for (int i = 0; i < _rows; i++) {
        int t_cols = _cols;
        if (n_cols < _cols) {
            while (n_cols != t_cols) {
                _matrix[i].pop_back();
                t_cols--;
            }
        } else if (n_cols > _cols) {
            while (n_cols != t_cols) {
                _matrix[i].push_back(0.0);
                t_cols++;
            }
        }
    }
    _cols = n_cols;
}

S21Matrix S21Matrix::operator+(const S21Matrix& other) {
    S21Matrix result = *this;
    result.sum_matrix(other);
    return result;
}

S21Matrix S21Matrix::operator-(const S21Matrix& other) {
    S21Matrix result = *this;
    result.sub_matrix(other);
    return result;
}

S21Matrix S21Matrix::operator*(const S21Matrix& other) {
    S21Matrix result = *this;
    result.mul_matrix(other);
    return result;
}

S21Matrix S21Matrix::operator*(const double num) {
    S21Matrix result = *this;
    result.mul_number(num);
    return result;
}

bool S21Matrix::operator==(const S21Matrix& other) { return eq_matrix(other); }

S21Matrix& S21Matrix::operator=(const S21Matrix& other) {
    if (this != &other) {
        s21_copy(other);
    }
    return *this;
}

void S21Matrix::operator+=(const S21Matrix& other) { return sum_matrix(other); }

void S21Matrix::operator-=(const S21Matrix& other) { return sub_matrix(other); }

void S21Matrix::operator*=(const S21Matrix& other) { return mul_matrix(other); }

void S21Matrix::operator*=(const double num) { return mul_number(num); }

double& S21Matrix::operator()(int n_row, int n_col) {
    if (_rows <= n_row || _cols <= n_col || n_row < 0 || n_col < 0) {
        throw out_of_range(messages(5));
    }
    return _matrix[n_row][n_col];
}

long double S21Matrix::s21_fabs(double x) {
    long double res = x;
    if (x < 0.0) res = -res;
    return res;
}

int S21Matrix::s21_1_or_minus_1(int n) {
    int res = -1;
    if (n % 2 == 0) res = 1;
    return res;
}

S21Matrix S21Matrix::s21_minor_matrix(int n, int m) {
    S21Matrix res(_rows - 1, _cols - 1);
    int i_now = 0, j_now = 0;
    for (int i = 0; i < _rows; i++) {
        if (i != n) {
            for (int j = 0; j < _cols; j++) {
                if (i != n && j != m) {
                    res._matrix[i_now][j_now] = _matrix[i][j];
                    j_now++;
                }
            }
            j_now = 0;
            i_now++;
        }
    }
    return res;
}

void S21Matrix::s21_sum_sub_mult_matrix(const S21Matrix& other, int op, double num) {
    s21_check_for_volid(other, J_OP_PLUS);
    for (int i = 0; i < _rows; i++) {
        for (int j = 0; j < _cols; j++) {
            if (op == SUB) {
                _matrix[i][j] = _matrix[i][j] - other._matrix[i][j];
            } else if (op == SUM) {
                _matrix[i][j] = _matrix[i][j] + other._matrix[i][j];
            } else if (op == MULT) {
                _matrix[i][j] = _matrix[i][j] * num;
            }
        }
    }
}

string S21Matrix::messages(int num) {
    string mes = "";
    if (num == 0) {
        mes = "Matrix must be square";
    } else if (num == 1) {
        mes = "Count of Rows and Columns of both matrices must be equal";
    } else if (num == 2) {
        mes = "Count of Columns must be equal to the count of Rows other matrix";
    } else if (num == 3) {
        mes = "Rows can't equal to 1";
    } else if (num == 4) {
        mes = "Determinant can't equal to 0";
    } else if (num == 5) {
        mes = "Index out of range";
    } else if (num == 6) {
        mes = "Cols must be above 0";
    } else if (num == 7) {
        mes = "Rows must be above 0";
    } else if (num == 8) {
        mes = "Rows and Columns must be have a value above 0";
    }
    return mes;
}

bool S21Matrix::s21_check_square() {
    bool res = (_rows == _cols);
    if (!res) throw invalid_argument(messages(0));
    return res;
}

bool S21Matrix::s21_check_for_volid(const S21Matrix& other, int check_option) {
    int res = true;
    if (check_option == J_OP || check_option == J_OP_PLUS) {
        res = ((_cols == other._cols) && (_rows == other._rows));
        if (!res && check_option == J_OP_PLUS) {
            throw invalid_argument(messages(1));
        }
    } else if (check_option == MATRIX_MULT_OP) {
        res = (_cols == other._rows);
        if (!res) throw invalid_argument(messages(2));
    } else if (check_option == SQUARE) {
        res = s21_check_square();
    } else if (check_option == CALC) {
        res = s21_check_square() && _rows != 1;
        if (!res) throw invalid_argument(messages(3));
    } else if (check_option == INVERSE) {
        res = s21_check_square() && determinant() != 0;
        if (!res) throw invalid_argument(messages(4));
    }
    return res;
}

void S21Matrix::s21_copy(const S21Matrix& other) {
    _rows = other._rows;
    _cols = other._cols;
    _matrix = other._matrix;
}
