#include <gtest/gtest.h>
#include <vector>

#include "s21_matrix.h"

TEST(create_matrix, S21Matrixests) {
    S21Matrix A(3, 3);
    int res = 1;
    for (int i = 0; i < A.get_rows() && res == 1; i++) {
        for (int j = 0; j < A.get_cols() && res == 1; j++) {
            if (A(i, j) != 0.0) res = 0;
        }
    }
    EXPECT_EQ(res, 1);
}

TEST(create_matrix_fail, S21Matrixests) {
    EXPECT_ANY_THROW(S21Matrix A(-1, 3));
}

TEST(create_matrix_move, S21Matrixests) {
    vector<S21Matrix> v;
    v.push_back(S21Matrix(1, 1));
    v.push_back(S21Matrix(1, 1));
    v.insert(v.begin() + 1, S21Matrix(2, 2));
    EXPECT_EQ(0, v[1](1, 1));
}

TEST(create_matrix_free, S21Matrixests) {
    S21Matrix A;
    int res = 1;
    for (int i = 0; i < A.get_rows() && res == 1; i++) {
        for (int j = 0; j < A.get_cols() && res == 1; j++) {
            if (A(i, j) != 0.0) res = 0;
        }
    }
    EXPECT_EQ(res, 1);
}

TEST(eq_matrix_fail, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix B(2, 2);
    EXPECT_EQ(false, A.eq_matrix(B));
}

TEST(eq_matrix, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix B(3, 3);
    EXPECT_EQ(true, A.eq_matrix(B));
}

TEST(sum_matrix, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix B(3, 3);
    A.sum_matrix(B);
    EXPECT_EQ(true, A.eq_matrix(B));
}

TEST(sum_matrix_operator, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix B(3, 3);
    S21Matrix C = A + B;
    EXPECT_EQ(true, A == C);
}

TEST(sum_matrix_fail, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix B(2, 2);
    EXPECT_ANY_THROW(S21Matrix C = A + B;);
}

TEST(sub_matrix, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix B(3, 3);
    A.sub_matrix(B);
    EXPECT_EQ(true, A.eq_matrix(B));
}

TEST(sub_matrix_operator, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix B(3, 3);
    S21Matrix C = A - C;
    EXPECT_EQ(true, A == C);
}

TEST(mult_matrix, S21Matrixests) {
    S21Matrix A(3, 2);
    S21Matrix B(2, 3);
    S21Matrix check(3, 3);
    A(0, 0) = 1; A(0, 1) = 4;
    A(1, 0) = 2; A(1, 1) = 5;
    A(2, 0) = 3; A(2, 1) = 6;
    B(0, 0) = 1; B(0, 1) = -1; B(0, 2) = 1;
    B(1, 0) = 2; B(1, 1) = 3; B(1, 2) = 4;
    check(0, 0) = 9; check(0, 1) = 11; check(0, 2) = 17;
    check(1, 0) = 12; check(1, 1) = 13; check(1, 2) = 22;
    check(2, 0) = 15; check(2, 1) = 15; check(2, 2) = 27;
    A.mul_matrix(B);
    EXPECT_EQ(true, A.eq_matrix(check));
}

TEST(mult_matrix_fail, S21Matrixests) {
    S21Matrix A(3, 2);
    S21Matrix B(3, 2);
    EXPECT_ANY_THROW(S21Matrix C = A * B);
}

TEST(mult_matrix_operaotr, S21Matrixests) {
    S21Matrix A(3, 2);
    S21Matrix B(2, 3);
    S21Matrix check(3, 3);
    A(0, 0) = 1; A(0, 1) = 4;
    A(1, 0) = 2; A(1, 1) = 5;
    A(2, 0) = 3; A(2, 1) = 6;
    B(0, 0) = 1; B(0, 1) = -1; B(0, 2) = 1;
    B(1, 0) = 2; B(1, 1) = 3; B(1, 2) = 4;
    check(0, 0) = 9; check(0, 1) = 11; check(0, 2) = 17;
    check(1, 0) = 12; check(1, 1) = 13; check(1, 2) = 22;
    check(2, 0) = 15; check(2, 1) = 15; check(2, 2) = 27;
    S21Matrix C = A * B;
    EXPECT_EQ(true, C == check);
}

TEST(mult_number, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix C(3, 3);
    A.mul_number(1);
    EXPECT_EQ(true, A.eq_matrix(C));
}

TEST(mult_number_operator, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix C = A * 1;
    EXPECT_EQ(true, A == C);
}

TEST(mult_number_operator_1, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix C(3, 3);
    A *= 1;
    EXPECT_EQ(true, A == C);
}

TEST(mult_matrix_operator_1, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix C(3, 3);
    A *= C;
    EXPECT_EQ(true, A == C);
}

TEST(sum_number_operator_1, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix C(3, 3);
    A += C;
    EXPECT_EQ(true, A == C);
}

TEST(sub_number_operator_1, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix C(3, 3);
    A -= C;
    EXPECT_EQ(true, A == C);
}

TEST(mult_matrices_operator, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix B(3, 3);
    S21Matrix C = A * B;
    EXPECT_EQ(true, A == C);
}

TEST(transpose, S21Matrixests) {
    S21Matrix A(3, 2);
    S21Matrix check(2, 3);
    A(0, 0) = 1; A(0, 1) = 4;
    A(1, 0) = 2; A(1, 1) = 5;
    A(2, 0) = 3; A(2, 1) = 6;
    check(0, 0) = 1; check(0, 1) = 2; check(0, 2) = 3;
    check(1, 0) = 4; check(1, 1) = 5; check(1, 2) = 6;
    S21Matrix C = A.transpose();
    EXPECT_EQ(true, C.eq_matrix(check));
}

TEST(determinant, S21Matrixests) {
    S21Matrix A(3, 3);
    A(0, 0) = 1; A(0, 1) = 2; A(0, 2) = 3;
    A(1, 0) = 4; A(1, 1) = 5; A(1, 2) = 6;
    A(2, 0) = 7; A(2, 1) = 8; A(2, 2) = 9;
    EXPECT_EQ(0, A.determinant());
}

TEST(determinant_fail, S21Matrixests) {
    S21Matrix A(2, 3);
    A(0, 0) = 1; A(0, 1) = 2; A(0, 2) = 3;
    A(1, 0) = 4; A(1, 1) = 5; A(1, 2) = 6;
    EXPECT_ANY_THROW(A.determinant());
}

TEST(determinant_1, S21Matrixests) {
    S21Matrix A(1, 1);
    A(0, 0) = 10;
    EXPECT_EQ(10, A.determinant());
}

TEST(calc_complements, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix check(3, 3);
    A(0, 0) = 1; A(0, 1) = 2; A(0, 2) = 3;
    A(1, 0) = 0; A(1, 1) = 4; A(1, 2) = 2;
    A(2, 0) = 5; A(2, 1) = 2; A(2, 2) = 1;
    check(0, 0) = 0; check(0, 1) = 10; check(0, 2) = -20;
    check(1, 0) = 4; check(1, 1) = -14; check(1, 2) = 8;
    check(2, 0) = -8; check(2, 1) = -2; check(2, 2) = 4;
    S21Matrix C = A.calc_complements();
    EXPECT_EQ(true, C.eq_matrix(check));
}

TEST(calc_complements_fail_2, S21Matrixests) {
    S21Matrix A(1, 1);
    EXPECT_ANY_THROW(A.calc_complements(););
}

TEST(calc_complements_fail_1, S21Matrixests) {
    S21Matrix A(2, 3);
    A(0, 0) = 1; A(0, 1) = 2; A(0, 2) = 3;
    A(1, 0) = 0; A(1, 1) = 4; A(1, 2) = 2;
    EXPECT_ANY_THROW(A.calc_complements());
}

TEST(inverse_matrix, S21Matrixests) {
    S21Matrix A(3, 3);
    S21Matrix check(3, 3);
    A(0, 0) = 2; A(0, 1) = 5; A(0, 2) = 7;
    A(1, 0) = 6; A(1, 1) = 3; A(1, 2) = 4;
    A(2, 0) = 5; A(2, 1) = -2; A(2, 2) = -3;
    check(0, 0) = 1; check(0, 1) = -1; check(0, 2) = 1;
    check(1, 0) = -38; check(1, 1) = 41; check(1, 2) = -34;
    check(2, 0) = 27; check(2, 1) = -29; check(2, 2) = 24;
    S21Matrix C = A.inverse_matrix();
    EXPECT_EQ(true, C.eq_matrix(check));
}

TEST(inverse_matrix_1, S21Matrixests) {
    S21Matrix A(1, 1);
    S21Matrix check(1, 1);
    A(0, 0) = 2;
    check(0, 0) = 1.0 / 2.0;
    S21Matrix C = A.inverse_matrix();
    EXPECT_EQ(true, C.eq_matrix(check));
}

TEST(inverse_matrix_fail_1, S21Matrixests) {
    S21Matrix A(2, 3);
    S21Matrix check(3, 3);
    A(0, 0) = 2; A(0, 1) = 5; A(0, 2) = 7;
    A(1, 0) = 6; A(1, 1) = 3; A(1, 2) = 4;
    EXPECT_ANY_THROW(A.inverse_matrix());
}

TEST(inverse_matrix_fail_2, S21Matrixests) {
    S21Matrix A(3, 3);
    EXPECT_ANY_THROW(A.inverse_matrix());
}

TEST(set_rows_1, S21Matrixests) {
    S21Matrix A(2, 3);
    S21Matrix C(1, 3);
    C(0, 0) = A(0, 0) = 2; C(0, 1) = A(0, 1) = 5; C(0, 2) = A(0, 2) = 7;
    A(1, 0) = 6; A(1, 1) = 3; A(1, 2) = 4;
    A.set_rows(1);
    EXPECT_EQ(true, A.eq_matrix(C));
}

TEST(set_rows_2, S21Matrixests) {
    S21Matrix A(2, 3);
    S21Matrix C(3, 3);
    C(0, 0) = A(0, 0) = 2; C(0, 1) = A(0, 1) = 5; C(0, 2) = A(0, 2) = 7;
    C(1, 0) = A(1, 0) = 6; C(1, 1) = A(1, 1) = 3; C(1, 2) = A(1, 2) = 4;
    C(2, 0) = 0; C(2, 1) = 0; C(2, 2) = 0;
    A.set_rows(3);
    EXPECT_EQ(true, A.eq_matrix(C));
}

TEST(set_cols_1, S21Matrixests) {
    S21Matrix A(2, 3);
    S21Matrix C(2, 2);
    C(0, 0) = A(0, 0) = 2; C(0, 1) = A(0, 1) = 5; A(0, 2) = 7;
    C(1, 0) = A(1, 0) = 6; C(1, 1) = A(1, 1) = 3; A(1, 2) = 4;
    A.set_cols(2);
    EXPECT_EQ(true, A.eq_matrix(C));
}

TEST(set_cols_2, S21Matrixests) {
    S21Matrix A(2, 3);
    S21Matrix C(2, 4);
    C(0, 0) = A(0, 0) = 2; C(0, 1) = A(0, 1) = 5; C(0, 2) = A(0, 2) = 7;
    C(0, 3) = 0;
    C(1, 0) = A(1, 0) = 6; C(1, 1) = A(1, 1) = 3; C(1, 2) = A(1, 2) = 4;
    C(1, 3) = 0;
    A.set_cols(4);
    EXPECT_EQ(true, A.eq_matrix(C));
}

TEST(get_element, S21Matrixests) {
    S21Matrix A(2, 3);
    EXPECT_ANY_THROW(A(-1, -1));
}

int main() {
  testing::InitGoogleTest();
  return RUN_ALL_TESTS();
}
