#ifndef SRC_S21_MATRIX_H_
#define SRC_S21_MATRIX_H_

#include <iostream>
#include <vector>

using std::vector;
using std::string;
using std::invalid_argument;
using std::out_of_range;

#ifndef SUB
#define SUB 0
#endif
#ifndef SUM
#define SUM 1
#endif
#ifndef MULT
#define MULT 2
#endif
#ifndef EPS
#define EPS 0.0000000999
#endif

#define J_OP 0
#define MATRIX_MULT_OP 1
#define CORRECT_ONLY -1
#define SQUARE 2
#define CALC 3
#define INVERSE 4
#define J_OP_PLUS 5

class S21Matrix {
 private:
    int _rows, _cols;
    vector<vector<double> > _matrix;

    void s21_create_matrix();
    bool s21_check_for_volid(const S21Matrix& other, int check_option);
    void s21_sum_sub_mult_matrix(const S21Matrix& other, int op, double num);
    S21Matrix s21_minor_matrix(int n, int m);
    int s21_1_or_minus_1(int n);
    long double s21_fabs(double x);
    bool s21_check_square();
    void s21_copy(const S21Matrix& other);
    string messages(int num);

 public:
    S21Matrix();
    S21Matrix(int rows, int cols);
    S21Matrix(const S21Matrix& other);
    S21Matrix(S21Matrix&& other);
    ~S21Matrix();
    bool eq_matrix(const S21Matrix& other);
    void sum_matrix(const S21Matrix& other);
    void sub_matrix(const S21Matrix& other);
    void mul_number(const double num);
    void mul_matrix(const S21Matrix& other);
    S21Matrix transpose();
    S21Matrix calc_complements();
    double determinant();
    S21Matrix inverse_matrix();

    int get_rows();
    int get_cols();
    void set_rows(int n_rows);
    void set_cols(int n_cols);

    S21Matrix operator+(const S21Matrix& other);
    S21Matrix operator-(const S21Matrix& other);
    S21Matrix operator*(const S21Matrix& other);
    S21Matrix operator*(const double num);
    bool operator==(const S21Matrix& other);
    S21Matrix& operator=(const S21Matrix& other);
    void operator+=(const S21Matrix& other);
    void operator-=(const S21Matrix& other);
    void operator*=(const S21Matrix& other);
    void operator*=(const double num);
    double& operator()(int n_row, int n_col);
};

#endif  // SRC_S21_MATRIX_H_
